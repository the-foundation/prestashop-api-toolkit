<?php

function PS_get_product_features_index($psurl,$auth_key) {

// Initialize variables

$resource = 'product_features';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
$display = 'full'; // Retrieve all fields, including product features

// Initialize cURL
$ch = curl_init();

// Set cURL options
curl_setopt($ch, CURLOPT_URL, $targurl );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Basic '. base64_encode($auth_key .':'),
    'Content-Type: application/xml'
));

// Make the cURL request and get the response
$response = curl_exec($ch);

// Check for errors
if (curl_errno($ch)) {
    myerror( 'Error: ' . curl_error($ch));
    return array();
}

// Close cURL
curl_close($ch);

// Convert the XML response to an associative array
//print_r($response$);

$featurelist = json_decode(json_encode(simplexml_load_string($response)), true);
$retlist=array();
foreach ($featurelist["product_features"]["product_feature"] as $featindex) {
    //print($featindex["@attributes"]["id"]);
    array_push($retlist,$featindex["@attributes"]["id"]);
}
//print_r($featurelist);

return $retlist;
}


function PS_get_product_features_full($psurl,$auth_key) {
// Initialize variables

$resource = 'product_features';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
$display = 'full'; // Retrieve all fields, including product features

$list=PS_get_product_features_index($psurl,$auth_key);
$retlist=array();
if(is_array($list)) {
    foreach($list as $curfeature) {
        // Initialize cURL
        $ch = curl_init();
        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, $targurl."/".$curfeature );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic '. base64_encode($auth_key .':'),
            'Content-Type: application/xml'
        ));
        // Make the cURL request and get the response
        $response = curl_exec($ch);
        // Check for errors
        if (curl_errno($ch)) {
            myerror( 'Error: ' . curl_error($ch));
        }
        // Close cURL
        curl_close($ch);
        // Convert the XML response to an associative array
        //print_r($response);

        $feature = json_decode(json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($feature["product_feature"])) {
            $returnelement=$feature["product_feature"];
            if(isset($returnelement["name"]["language"])) {
                $newname=$returnelement["name"]["language"];
                $returnelement["name"]=$newname;
                }
            array_push($retlist,$returnelement);
        }

        //print_r($feature["product_feature"]);

        //$retlist=array();
        //foreach ($featurelist["product_features"]["product_feature"] as $featindex) {
        //    //print($featindex["@attributes"]["id"]);
        //
        //}
    } // end foreach curfeature
} //end is_array

//print_r($featurelist);

return $retlist;
}

function PS_get_product_feature_values_index($psurl,$auth_key) {

// Initialize variables

$resource = 'product_feature_values';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
$display = 'full'; // Retrieve all fields, including product features

// Initialize cURL
$ch = curl_init();

// Set cURL options
curl_setopt($ch, CURLOPT_URL, $targurl );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Basic '. base64_encode($auth_key .':'),
    'Content-Type: application/xml'
));

// Make the cURL request and get the response
$response = curl_exec($ch);

// Check for errors
if (curl_errno($ch)) {
    myerror( 'Error: ' . curl_error($ch));
    return array();
}

// Close cURL
curl_close($ch);

// Convert the XML response to an associative array
//print_r($response);

$featurelist = json_decode(json_encode(simplexml_load_string($response)), true);
$retlist=array();
foreach ($featurelist["product_feature_values"]["product_feature_value"] as $featindex) {
    //print($featindex["@attributes"]["id"]);
    array_push($retlist,$featindex["@attributes"]["id"]);
}
//print_r($featurelist);

return $retlist;
}


function PS_get_product_feature_values_full($psurl,$auth_key) {
// Initialize variables

$resource = 'product_feature_values';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
$display = 'full'; // Retrieve all fields, including product features

$list=PS_get_product_feature_values_index($psurl,$auth_key);
$retlist=array();
if(is_array($list)) {
    foreach($list as $curfeature) {
        // Initialize cURL
        $ch = curl_init();
        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, $targurl."/".$curfeature );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic '. base64_encode($auth_key .':'),
            'Content-Type: application/xml'
        ));
        // Make the cURL request and get the response
        $response = curl_exec($ch);
        // Check for errors
        if (curl_errno($ch)) {
            myerror( 'Error: ' . curl_error($ch));
        }
        // Close cURL
        curl_close($ch);
        // Convert the XML response to an associative array
        //print_r($response);

        $feature = json_decode(json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($feature["product_feature_value"])) {
            $returnelement=$feature["product_feature_value"];
            if(isset($returnelement["value"]["language"])) {
                $newname=$returnelement["value"]["language"];
                $returnelement["name"]=$newname;
                unset($returnelement["value"]);
                }
            array_push($retlist,$returnelement);
        }

        //print_r($feature["product_feature_value"]);

        //$retlist=array();
        //foreach ($featurelist["product_feature_values"]["product_feature"] as $featindex) {
        //    //print($featindex["@attributes"]["id"]);
        //
        //}
    } // end foreach curfeature
} //end is_array

//print_r($featurelist);

return $retlist;
}
