#!/bin/bash 
## use me like this:
## read PRESTADOMAIN;read PRESTA_TOKEN;source _get_schema.sh
test -e schema ||mkdir schema
for thingy in images addresses attachments carriers cart_rules carts categories combinations configurations contacts content_management_system countries currencies customer_messages customer_threads customers customizations deliveries employees groups guests image_types languages manufacturers messages order_carriers order_details order_histories order_invoices order_payments order_slip order_states orders price_ranges product_customization_fields product_feature_values product_features product_option_values product_options product_suppliers products search shop_groups shop_urls shops specific_price_rules specific_prices states stock_availables stock_movement_reasons stock_movements stocks stores suppliers supply_order_details supply_order_histories supply_order_receipt_histories supply_order_states supply_orders tags tax_rule_groups tax_rules taxes translated_configurations warehouse_product_locations warehouses weight_ranges zones;do 
    test -e "schema/$thingy.xml" || ( curl -u $PRESTA_TOKEN:asd 'https://'$PRESTADOMAIN'/api/'$thingy'?schema=blank' > "schema/$thingy.xml" ) ;
    file "schema/$thingy.xml" |grep -q "XML"|| rm "schema/$thingy.xml"
done

for thingytwo in attachments/file images/general/header images/general/mail images/general/invoice images/general/store_icon images/products images/categories images/manufacturers images/suppliers images/stores images/customizations ; do
mkdir -p schema/$(dirname "$thingytwo")
test -e "schema/$thingytwo.xml"||   (curl -u $PRESTA_TOKEN:asd 'https://'$PRESTADOMAIN'/api/'$thingytwo'?schema=blank' > "schema/$thingytwo.xml");
file "schema/$thingytwo.xml"|grep XML -q || rm "schema/$thingytwo.xml"

done
