<?php

function PS_get_product_image_list($shop_path,$auth_key,$prodID) {
    
try {
    $webService = new PrestaShopWebservice($shop_path,$auth_key, false);
    $opt=array();
    $opt['resource'] = 'images/products/'.$prodID ;
    $opt['display'] = 'full';
    //$opt['limit'] = 10000;

    $xml = $webService->get($opt);
    $assocresp=json_decode(json_encode(($xml), true), JSON_PRETTY_PRINT);
    unset($outwebService);

    //print_r($assocresp["image"]["@attributes"]["id"]);
    $returnResponse=array();
    foreach($assocresp["image"]["declination"] as $imgres ) {
        array_push($returnResponse,$imgres["@attributes"]["id"]);
    }
    //print_r($returnResponse);
    
//    $resources = $xml->image_types->children();
    //$resources = $xml->children();
    //$imageTypes = $xml->image_types->children();
    //$images = $xml->image->declination->children();

    //print($xml);
    //print_r($images);
    //$dec=$resources->image->declination;
    //$resarr=array($resources);
    //print_r($resarr);
    //foreach ($resources->image->declination AS $dec) {
    //     print_r($dec->attributes->id);
    // }
    return $returnResponse;
} catch (PrestaShopWebserviceException $e) {
    echo 'Error:' . $e->getMessage();
}
  
}

function PS_get_product_image_file($shop_path,$auth_key,$prodID,$imageID,$imageSize="home_default") {
    
    // thy https://github.com/Silentscripter/laravel-prestashop-webservice/issues/13
    // Did you mean: "home_default"? //The full list is: "cart_default", "home_default", "large_default", "medium_default", "small_default"
try {
    if ( !in_array($imageSize,array( "cart_default", "home_default", "large_default", "medium_default", "small_default" )) ) {
        $imageSize="home_default";        
    }
    $webServiceUrl = $shop_path . 'api/images/products/'. $prodID.'/'.$imageID.'/home_default' ; ///'. $prodID.'/'.$imageSize; //If you must update an existing image you must attach "/{$imageID}/?ps_method=PUT" at the end of the url
    $curlChannel = curl_init();
    curl_setopt($curlChannel, CURLOPT_URL, $webServiceUrl);
    curl_setopt($curlChannel, CURLOPT_POST, false);
    curl_setopt($curlChannel, CURLOPT_USERPWD, $auth_key . ':'); //note token must be followed by a colon in this situation
    curl_setopt($curlChannel, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curlChannel);
    $info = curl_getinfo($curlChannel);
    curl_close($curlChannel);
    switch ($info['http_code']) {
    case 200:
       return $response;
   default:
        throw new \Exception($response, $info['http_code']);
        break;
}
    //$inwebService = new PrestaShopWebservice($shop_path,$auth_key , DEBUG);
} catch (PrestaShopWebserviceException $e) {
    echo 'Error:' . $e->getMessage();
}
}

function PS_get_category_image_file($shop_path,$auth_key,$categoryID,$imageSize="category_default") {
    
    // thy https://github.com/Silentscripter/laravel-prestashop-webservice/issues/13
    // Did you mean: "small_default"? The full list is: "category_default", "small_default"
try {
    //if ( !in_array($imageSize,array( "cart_default", "home_default", "large_default", "medium_default", "small_default" )) ) {
    if ( !in_array($imageSize,array( "category_default", "small_default" )) ) {
        $imageSize="category_default";        
    }
    $webServiceUrl = $shop_path . 'api/images/categories/'. $categoryID.'/'.$imageSize; //If you must update an existing image you must attach "/{$imageID}/?ps_method=PUT" at the end of the url
    $curlChannel = curl_init();
    curl_setopt($curlChannel, CURLOPT_URL, $webServiceUrl);
    curl_setopt($curlChannel, CURLOPT_POST, false);
    curl_setopt($curlChannel, CURLOPT_USERPWD, $auth_key . ':'); //note token must be followed by a colon in this situation
    curl_setopt($curlChannel, CURLOPT_RETURNTRANSFER, true);
    $response = curl_exec($curlChannel);
    $info = curl_getinfo($curlChannel);
    curl_close($curlChannel);
    switch ($info['http_code']) {
    case 200:
       return $response;
   default:
        throw new \Exception($response, $info['http_code']);
        break;
}
    //$inwebService = new PrestaShopWebservice($shop_path,$auth_key , DEBUG);
} catch (PrestaShopWebserviceException $e) {
    echo 'Error:' . $e->getMessage();
}
}
