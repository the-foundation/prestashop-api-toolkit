<?php

require_once("presta-functions-product_features.php");
require_once("presta-functions-category-create.php");
require_once("presta-functions-category-get.php");
require_once("presta-functions-category-path.php");
require_once("presta-functions-images.php");
require_once("presta-functions-manufacturer.php");
