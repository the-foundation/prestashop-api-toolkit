<?php
function PS_get_manufacturers_index($psurl,$auth_key) {

// Initialize variables

$resource = 'manufacturers';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
//$display = 'full'; // Retrieve all fields, including product features
// Initialize cURL
$ch = curl_init();

// Set cURL options
curl_setopt($ch, CURLOPT_URL, $targurl );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'Authorization: Basic '. base64_encode($auth_key .':'),
    'Content-Type: application/xml'
));

// Make the cURL request and get the response
$response = curl_exec($ch);

// Check for errors
if (curl_errno($ch)) {
    myerror( 'Error: ' . curl_error($ch));
    return array();
}

// Close cURL
curl_close($ch);

// Convert the XML response to an associative array
//print_r($response);

$manufacturerlist = json_decode(json_encode(simplexml_load_string($response)), true);
//print_r($manufacturerlist);
$retlist=array();
foreach ($manufacturerlist["manufacturers"]["manufacturer"] as $mfgindex) {
    //print($mfgindex["id"]);
    //print_r($mfgindex);
    array_push($retlist,$mfgindex["id"]);
}
return $retlist;
}


function PS_get_manufacturers_full($psurl,$auth_key) {
// Initialize variables

$resource = 'manufacturers';
$targurl=str_replace('//api','/api', $psurl .  '/api/' . $resource);
$display = 'full'; // Retrieve all fields, including product features

$list=PS_get_manufacturers_index($psurl,$auth_key);
$retlist=array();
if(is_array($list)) {
    foreach($list as $curfeature) {
        // Initialize cURL
        $ch = curl_init();
        // Set cURL options
        curl_setopt($ch, CURLOPT_URL, $targurl."/".$curfeature );
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic '. base64_encode($auth_key .':'),
            'Content-Type: application/xml'
        ));
        // Make the cURL request and get the response
        $response = curl_exec($ch);
        // Check for errors
        if (curl_errno($ch)) {
            myerror( 'Error: ' . curl_error($ch));
        }
        // Close cURL
        curl_close($ch);
        // Convert the XML response to an associative array
        //print_r($response);

        $feature = json_decode(json_encode(simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($feature["manufacturer"])) {
            $returnelement=$feature["manufacturer"];
            if(isset($returnelement["name"]["language"])) {
                $newname=$returnelement["name"]["language"];
                $returnelement["name"]=$newname;
                }
            array_push($retlist,$returnelement);
        }
        //print_r($feature["manufacturer"]);

        //$retlist=array();
        //foreach ($manufacturerlist["manufacturers"]["manufacturer"] as $mfgindex) {
        //    //print($mfgindex["@attributes"]["id"]);
        //
        //}
    } // end foreach curfeature
} //end is_array

//print_r($manufacturerlist);

return $retlist;
}
