<?php

// joke is in the function , presta-flop crippled it's API :(
function PS_new_category($shop_path,$auth_key,$n_is_root_category, $n_id_parent, $n_active, $n_lang_id, $n_name, $n_desc, $n_link_rewrite, $n_meta_title, $n_meta_description, $n_meta_keywords) {

//$webService = new PrestaShopWebservice(PS_SHOP_PATH, PS_WS_AUTH_KEY, DEBUG);
//$webService = new PrestaShopWebservice($current_outgoing_shop['PS_SHOP_PATH'],$current_outgoing_shop['PS_WS_AUTH_KEY'] , DEBUG);
//$shop_path=$current_outgoing_shop['PS_SHOP_PATH']

//global $webService;
$webService = new PrestaShopWebservice($shop_path,$auth_key, DEBUG);
if(apcu_enabled()) {
   $cachekey="blankxml_cat_".$shop_path;
   if(apcu_exists($cachekey)) {
        $blankxml = apcu_fetch($cachekey);
   } else {
        $blankxml = $webService -> get(array('url' => $shop_path . '/api/categories?schema=blank'));
        apcu_store($cachekey, $blankxml, 1800);
   } 
} else {
   $blankxml = $webService -> get(array('url' => $shop_path . '/api/categories?schema=blank'));
}





$xml=$blankxml;

$resources = $xml -> children() -> children();
unset($resources -> id);
unset($resources -> position);
unset($resources -> id_shop_default);
unset($resources -> date_add);
unset($resources -> date_upd);

$resources -> active = $n_active;
//$resources -> id_parent = $n_id_parent;
//$resources -> id_parent['xlink:href'] = $shop_path . '/api/categories/' . $n_id_parent;
//USELESS DUE TO PRESTA-FLOP DAMAGED API ; WILL ALWAYS HAVE PARENT 1 .. THEY DONT EVEN HAVE TESTS IN THEIR REPO .. 
$resources -> id_parent = 1;

$resources -> is_root_category = $n_is_root_category;

$node = dom_import_simplexml($resources -> name -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_name));

$resources -> name -> language[0][0] = $n_name;
$resources -> name -> language[0][0]['id'] = $n_lang_id;
$resources -> name -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

$node = dom_import_simplexml($resources -> description -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_desc));

$resources -> description -> language[0][0] = $n_desc;
$resources -> description -> language[0][0]['id'] = $n_lang_id;
$resources -> description -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

$node = dom_import_simplexml($resources -> link_rewrite -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_link_rewrite));

$resources -> link_rewrite -> language[0][0] = $n_link_rewrite;
$resources -> link_rewrite -> language[0][0]['id'] = $n_lang_id;
$resources -> link_rewrite -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

$node = dom_import_simplexml($resources -> meta_title -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_meta_title));

$resources -> meta_title -> language[0][0] = $n_meta_title;
$resources -> meta_title -> language[0][0]['id'] = $n_lang_id;
$resources -> meta_title -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

$node = dom_import_simplexml($resources -> meta_description -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_meta_description));

$resources -> meta_description -> language[0][0] = $n_meta_description;
$resources -> meta_description -> language[0][0]['id'] = $n_lang_id;
$resources -> meta_description -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

$node = dom_import_simplexml($resources -> meta_keywords -> language[0][0]);
$no = $node -> ownerDocument;
$node -> appendChild($no -> createCDATASection($n_meta_keywords));

$resources -> meta_keywords -> language[0][0] = $n_meta_keywords;
$resources -> meta_keywords -> language[0][0]['id'] = $n_lang_id;
$resources -> meta_keywords -> language[0][0]['xlink:href'] = $shop_path . '/api/languages/' . $n_lang_id;

try {
$opt = array('resource' => 'categories');
$opt['postXml'] = $xml -> asXML();
$xml = $webService -> add($opt);
//return($xml->category->id);

if(isset($xml->category->id)) {
    $newid=$xml->category->id;
    $xml = $webService->get([
        'resource' => 'categories',
        'id' => $newid, 
    ]);
    $categoryfields = $xml->category->children();
    $categoryfields->id_parent=$n_id_parent;
    //unset($xml->level_depth);
    unset($categoryfields->level_depth);
    unset($categoryfields->nb_products_recursive);
    $updatedXml = $webService->edit([
        'resource' => 'categories',
        'id' => (int) $categoryfields->id,
        'putXml' => $xml->asXML(),
    ]);
    return $newid;
 } else {
   return false;
 }

} catch (PrestaShopWebserviceException $ex) {
echo("PS/SYNC CATEGORY: " . $ex -> getMessage()); // log function
}

unset($webService);
} // end function
