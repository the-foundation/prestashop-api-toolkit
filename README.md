# Prestashop-php-lib


## Installation:

* grab this repo via git
* get a copy of `https://github.com/PrestaShop/PrestaShop-webservice-lib` or use composer

* in your php file:
   * define the functions `myerror($string)` and `myecho($string)`
   * include PrestaShop-webservice-lib and presta-functions.php ( or only your needed functions) , example:
   ```
   require_once('./PrestaShop-webservice-lib/PSWebServiceLibrary.php');
   require_once('./prestashop-api-toolkit/presta-functions.php');
   ```

### Data format

   ### Lists
   |Function| Returns|
   |---|---|
   | PS_get_product_features_index , PS_get_product_features_index | array of strings|
   | PS_get_product_feature_values_full , PS_get_product_features_full ,PS_get_manufactureres_full | associative arrays |

* functions `PS_get_product_feature_values_full` and `PS_get_product_features_full` return arrays with `name` property

   ![](README_assets/PS_get_product_feature_values_full.png)


## Reference:
* you will find blank xml Schema files in `reference/schema` all from a working presta installation
* there is a generator script in the `reference` folder


---

<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/prestashop-api-toolkit/README.md/logo.jpg" width="480" height="270"/></div></a>


## Todo
- [ ] MORE DOCUMENTATION
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
