<?php

function PS_get_categories_all_full($shop_path,$auth_key) {
    try
    {
    $outwebService = new PrestaShopWebservice($shop_path,$auth_key , DEBUG);

        // Here we set the option array for the Webservice : we want customers resources
        //$opt['resource'] = 'customers';
        $opt['resource'] = 'categories';
        $opt['display'] = 'full';
        $opt['limit'] = 10000;
        // Call
        $xml = $outwebService->get($opt);

        // Here we get the elements from children of categories markup "categories"
        $resources = $xml->categories->children();
    unset($outwebService);
    }
    catch (PrestaShopWebserviceException $e)
    {
        // Here we are dealing with errors
        $trace = $e->getTrace();
        echo('Error '.json_encode($trace));
        //if ($trace[0]['args'][0] == 404) echo('Bad ID '.$shop_path);
        //else if ($trace[0]['args'][0] == 401) echo('Bad auth key '.$shop_path);
        //else echo('Other error '.$shop_path);
    }
    if(isset($resources)) {return $resources ;} else {return "FAIL";}

}

function PS_search_category_by_id($needle,$haystack) {
    $id_found=-1;
    foreach ($haystack AS $current_resource) 
    {
        if($current_resource["id"]==$needle) {
            return $current_resource;
        }

    }
    return $id_found;
}

function PS_search_category_by_name($needle,$haystack) {
    $id_found=-1;
    foreach ($haystack AS $current_resource) 
    {
        if($current_resource["name"]["language"]==$needle) {
            return $current_resource["id"];
        }

    }
    return $id_found;
}

function PS_search_category_by_permalink($needle,$haystack) {
    $id_found=-1;
    foreach ($haystack AS $current_resource) 
    {
        if($current_resource["link_rewrite"]["language"]==$needle) {
            return $current_resource["id"];
        }

    }
    return $id_found;
}


